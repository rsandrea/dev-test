<?php 
namespace Action;

use Db\Handler;
use Model\Person;

class Delete
{
    /**
     * public property that will store where the request_handler
     * should redirect to
     * @var string
     */
    public $redirect;

    /**
     * It will setup the Db handler, delete and return an action
     * to redirect to
     * 
     * @param array $post
     */
    public function __construct($post)
    {

        $db = new Handler(array('password' => '14863993'));
        $person = new Person($db);

        $id = $post['id'];
        
        $this->redirect = $person->delete($id);
    }
}