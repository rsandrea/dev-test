<?php
namespace Action;

use Db\Handler;
use Model\Person;

class Retrieve
{
    /**
     * public property that will store where the request_handler
     * should redirect to
     * @var string
     */
    public $redirect;

    /**
     * Private property that will store an instance of the 
     * db handler
     * @var Db\Handler
     */
    private $db;

    /**
     * It will construct an instance of the Retriever action
     */
    public function __construct()
    {
        $this->db = new Handler(array('password' => '14863993'));
    }

    /**
     * It returns an array of DB rows
     * 
     * @return array
     */
    public function fetchAll()
    {
        $person = new Person($this->db);

        return $person->fetchAll();
    }

    /**
     * Returns an array containing an specific row content
     * 
     * @param integer $id
     * @return array
     */
    public function find($id)
    {
        $person = new Person($this->db);

        return $person->find($id);
    }
}