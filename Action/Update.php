<?php
namespace Action;

use Db\Handler;
use Model\Person;

class Update
{
    /**
     * public property that will store where the request_handler
     * should redirect to
     * @var string
     */
    public $redirect;

    /**
     * Creates an instance of the update action
     * 
     * @param array $post
     */
    public function __construct($post)
    {

        $db = new Handler(array('password' => '14863993'));
        $person = new Person($db);

        $id = $post['id'];
        unset($post['id']);

        $this->redirect = $person->update($post, $id);
    }
}