<?php
namespace Action;

use Db\Handler;
use Model\Person;

class Store
{
    /**
     * public property that will store where the request_handler
     * should redirect to
     * @var string
     */
    public $redirect;

    /**
     * Creates an instance of the Store action
     * 
     * @param array $post
     */
    public function __construct($post)
    {

        $db = new Handler(array('password' => '14863993'));
        $person = new Person($db);

        $this->redirect = $person->save($post);
    }
}