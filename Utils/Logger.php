<?php
namespace Utils;

class Logger
{
    /**
     * Empty logger constructor, It may be used in the future
     * for further logger configuration
     */
    public function __construct()
    {}

    /**
     * It logs the provided message on the system log file
     * 
     * @param string $message
     */
    public function log($message) 
    {
        file_put_contents(__DIR__ . '/../logs/db.log', $message . "\n", FILE_APPEND);
    }
}