<?php
namespace Db;

Class Handler
{

    /**
     * PDO driver
     * @var string
     */
    private $type = 'mysql';

    /**
     * Db Host to connect to
     * @var string
     */
    private $host = '127.0.0.1';

    /**
     * Db Port
     * @var integer
     */
    private $port = 3306;

    /**
     * Db name
     * @var string
     */
    private $database = 'raul_sandrea_task';

    /**
     * Db username
     * @var string
     */
    private $user = 'root';

    /**
     * Db password
     * @var string
     */
    private $password;

    /**
     * Db\Handler instance
     * @var Db\Handler
     */
    private $connection;

    /**
     * Db Handler constructor
     * 
     * @param array $options
     */
    public function __construct($options = array())
    {
        foreach ($options as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     * It takes care of the Db connection
     * 
     * @return \PDO 
     */
    public function connect()
    {

        if ($this->type !== 'mysql') {
            throw new \Exception('Oops, the selected PDO type is not yet supported');
        }

        $dsn = $this->type . ':' . 'host=' . $this->host . ';' . 'port=' . $this->port . ';' . 'dbname=' . $this->database;

        try {
            $this->connection = new \PDO($dsn, $this->user, $this->password, array(
                \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ));
        } catch (\PDOException $e) {
            throw new \Exception('We could not connect to the DB, the reported message is: ' . $e->getMessage());
        }
    }

    /**
     * Returns an instance of an already instanced PDO object
     * and if it is not yet instanciated it creates it
     * 
     * @return \PDO
     */
    public function getConnection()
    {
        if (!$this->connection) {
            $this->connect();
        }

        return $this->connection;
    }

    /**
     * Returns an array of DB rows
     * 
     * @param string $sql
     * @return array
     */
    public function fetchAll($sql) 
    {
        $conn = $this->getConnection();

        try {
            return $conn->query($sql)->fetchAll();
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * Returns an array of a single row data
     * 
     * @param string $sql
     * @return array
     */
    public function fetchOne($sql)
    {
        $conn = $this->getConnection();

        try {
            return $conn->query($sql)->fetch();
        } catch (\Exception $e) {
            return array();
        }
    }

    /**
     * Executes an insert and returns the newly created ID
     * 
     * @param string $sql
     * @return integer
     */
    public function insert($sql)
    {
        $conn = $this->getConnection();

        $conn->exec($sql);
        return $conn->lastInsertId();
    }

    /**
     * Execute a random SQL against the connected DB
     * 
     * @param  string $sql
     * @return integer
     */
    public function exec($sql)
    {
        $conn = $this->getConnection();

        return $conn->exec($sql);
    }
}