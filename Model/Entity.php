<?php
namespace Model;

use Db\Handler;
use Utils\Logger;

class Entity
{

    /**
     * First Name
     * @var string
     */
    private $first_name;

    /**
     * Last Name
     * @var string
     */
    private $last_name;

    /**
     * Country
     * @var string
     */
    private $country;

    /**
     * City
     * @var integer
     */
    private $city;

    /**
     * Address
     * @var string
     */
    private $address;

    /**
     * Email
     * @var string
     */
    private $email;

    /**
     * Db Handler
     * @var Db\Handler
     */
    private $handler;

    /**
     * System logger
     * @var Utils\Logger
     */
    private $logger;

    /**
     * Constructor
     * 
     * @param Handler $handler
     */
    public function __construct(Handler $handler)
    {
        $this->handler = $handler;
        $this->logger = new Logger;
    }

    /**
     * It quote the key's to be used on a SQL query statement
     * 
     * @param string $element
     * @return string
     */
    public function prepareKeys($element) 
    {
        return "`" . $element . "`";
    }

    /**
     * It quote the values's to be used on a SQL query statement
     * 
     * @param string|array $element
     * @return string
     */
    public function prepareValues($element) 
    {
        if (is_array($element)) {
            return "'" . addslashes(implode('|', $element)) . "'";
        } else {
            return "'" . addslashes($element) . "'";
        }
    }

    /**
     * Returns an instance of the Db Handler
     * 
     * @return Db\Handler
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * Returns an instance of the system logger
     * @return Utils\Logger
     */
    public function getLogger()
    {
        return $this->logger;
    }
}