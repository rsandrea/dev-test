<?php 
/**
 * The Person entity describes the actions and properties
 * of a person
 * 
 * @author Raul Sandrea <raul.sandrea@gmail.com>
 * 
 */

namespace Model;

use Model\Entity;

class Person extends Entity
{

    /**
     * Person entity save funcion, it receives an array and 
     * return where the handler should redirect to
     * 
     * @param array $data
     * @return string
     */
    public function save($data)
    {
        unset($data['id']);

        $fields = array_map(array($this, 'prepareKeys'), array_keys($data));

        $values = array_map(array($this, 'prepareValues'), $data);

        $sql = sprintf('insert into person (%s) values (%s)', implode(',', $fields), implode(',', $values));

        try {
            if (!$this->getHandler()->insert($sql)) {
                throw new \Exception('Error while running ' . $sql);
            }
        } catch (\Exception $e) {
            print "There was a problem with the Db. Try again later";
            $this->getLogger()->log(date('Y-m-d H:i:s') . '[ERROR] -- ' . $e->getMessage());
        }

        return 'index';
    }

    /**
     * Person entity update, handles the update functionality
     * 
     * @param array $data
     * @param integer $id
     * @return string
     */
    public function update($data, $id)
    {
        $updateFields = array();
        foreach ($data as $key => $value) {
            $updateFields[] = $this->prepareKeys($key) . ' = ' . $this->prepareValues($value);
        }

        $sql = sprintf('update person set %s where `id` = %d', implode(',', $updateFields), $id);
        
        try {
            $this->getHandler()->exec($sql);
        } catch (\Exception $e) {
            print "There was a problem with the Db. Try again later";
            $this->getLogger()->log(date('Y-m-d H:i:s') . '[ERROR] -- ' . $e->getMessage());
        }

        return 'index';
    }

    /**
     * Person entity delete funcion
     * 
     * @param integer $id
     * @return string
     */
    public function delete($id)
    {
        $sql = sprintf('delete from person where `id` = %d', $id);
        
        try {
            $this->getHandler()->exec($sql);
        } catch (\Exception $e) {
            print "There was a problem with the Db. Try again later";
            $this->getLogger()->log(date('Y-m-d H:i:s') . '[ERROR] -- ' . $e->getMessage());
        }

        return 'index';
    }

    /**
     * Returns an array containing all the peron's table data
     * 
     * @return array
     */
    public function fetchAll()
    {
        return $this->getHandler()->fetchAll(
            "SELECT
            `id`,
            `first_name`,
            `last_name`,
            `country`,
            `city`,
            `address`,
            `email`
            FROM `person`"
        );
    }

    /**
     * Returns an array containing a single row's data
     * 
     * @param integer $id
     * @return array
     */
    public function find($id)
    {
        return $this->getHandler()->fetchOne(
            "SELECT
            `id`,
            `first_name`,
            `last_name`,
            `country`,
            `city`,
            `address`,
            `email`
            FROM `person` where `id` = '$id'"
        );
    }
}