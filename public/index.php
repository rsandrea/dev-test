<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Practical Task</title>
        <link rel="stylesheet" href="css/blue.css" type="text/css" />
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <link rel="stylesheet" href="css/jquery.tablesorter.pager.css" />
        <?php require 'init.php' ?>
    </head>
    <body>
        <h1>Practical Test</h1>
        <div id="links"><a href="add.php">Add a new record</a></div>
        <table id="home_sortable" cellspacing="1" class="tablesorter">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Country</th>
                    <th>City</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Options</th>
                </tr>
            </thead>
            <tbody>
                <?php 

                $countries = json_decode(file_get_contents(__DIR__ . '/js/countries.json'));

                $cities = json_decode(file_get_contents(__DIR__ . '/js/cities.json'));

                $r = new Action\Retrieve();
                foreach ($r->fetchAll() as $record): ?>
                <tr>
                    <td>
                        <?php echo $record['first_name'] ?>
                    </td>
                    <td>
                        <?php echo $record['last_name'] ?>
                    </td>
                    <td>
                        <?php echo $countries->{$record['country']} ?>
                    </td>
                    <td>
                        <?php echo $cities->{$record['country']}[$record['city']] ?>
                    </td>
                    <td>
                        <?php echo str_replace('|', '; ', $record['address']) ?>
                    </td>
                    <td>
                        <?php echo $record['email'] ?>
                    </td>
                    <td>
                        <a href="edit.php?id=<?php echo $record['id']?>">Edit</a>
                        <a href="delete.php?id=<?php echo $record['id']?>">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div id="pager" class="pager">
    <form>
        <img src="images/first.png" class="first"/>
        <img src="images/prev.png" class="prev"/>
        <input type="text" class="pagedisplay"/>
        <img src="images/next.png" class="next"/>
        <img src="images/last.png" class="last"/>
        <select class="pagesize">
            <option selected="selected" value="10">10</option>
            <option value="20">20</option>
            <option value="30">30</option>
        </select>
    </form>
</div>
    </body>

    <!-- Placed at the end of the document so the page load faster -->
    <script src="js/jquery.tablesorter.min.js"></script>
    <script src="js/jquery.tablesorter.pager.js"></script>
    <script type="text/javascript">
    $(document).ready(function() 
        { 
            $("#home_sortable").tablesorter({ 
                sortList: [[0,0]],
                headers: { 
                    6: { 
                        sorter: false 
                    } 
                }
            }).tablesorterPager({container: $("#pager")});
        } 
    ); 
    </script>

</html>