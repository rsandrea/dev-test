<?php
require 'init.php';

if (!empty($_POST)) {

    // print "<pre>" . print_r($_POST, true) . "</pre>";

    if (isset($_POST['action'])) {

        $actionName = $_POST['action'];
        unset($_POST['action']);

        $args = array(
            'first_name' => FILTER_SANITIZE_STRING,
            'last_name'  => FILTER_SANITIZE_STRING,
            'country'    => FILTER_SANITIZE_STRING,
            'city'       => FILTER_SANITIZE_STRING,
            'address'    => array(
                'filter' => FILTER_SANITIZE_STRING,
                'flags'  => FILTER_REQUIRE_ARRAY,
            ),
            'email'      => FILTER_SANITIZE_EMAIL,
            'id'         => FILTER_VALIDATE_INT,
        );

        $inputs = filter_input_array(INPUT_POST, $args);

        $action = new $actionName($inputs);

        echo '<link rel="stylesheet" href="css/style.css" type="text/css" />';

        echo '<meta http-equiv="refresh" content="5;URL=\'' . $action->redirect . '.php\'">';
        echo "<h1>Your operation have been successful!<h1>";
        echo "<h4>You will be redirected in 5 seconds</h4>";
    }
}