<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Practical Task - add a record</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />
        <?php require 'init.php' ?>
    </head>
    <body>
        <h1>Confirm record deletion</h1>
        <div id="links"><a href="index.php">&laquo; Back</a></div>

        <h3>Are you sure that you want to delete this record?</h3>
        <form action="request_handler.php" method="post" onsubmit="return validate(this);">
            <input type="hidden" name="id" value="<?php echo $_GET['id'] ?>" />
            <input type="hidden" name="action" value="Action\Delete" />
            <input type="submit" value="Yes" />
            <a href="index.php">Cancel</a>
        </form>
    </body>
</html>