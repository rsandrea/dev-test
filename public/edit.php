<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Practical Task - edit a record</title>
        <link rel="stylesheet" href="css/style.css" type="text/css" />

        <?php require 'init.php' ?>
    </head>
    <body>
        <?php 
        $action = new Action\Retrieve();
        $data = $action->find($_GET['id']);

        $address = explode('|', $data['address']);
        ?>
        <h1>Edit an existing record</h1>
        <div id="links"><a href="index.php">&laquo; Back</a></div>
        <form action="request_handler.php" method="post" onsubmit="return validate(this);">
            <label for="first_name">First Name</label>
            <input type="text" name="first_name" id="first_name" required value="<?php echo $data['first_name'] ?>" placeholder="First Name" />

            <label for="last_name">Last Name</label>
            <input type="text" name="last_name" id="last_name" required value="<?php echo $data['last_name'] ?>" placeholder="Last Name" />

            <label for="country">Country</label>
            <select name="country" id="country" required onchange="filterCity(this.value)">
            </select>

            <label for="city">City</label>
            <select name="city" id="city" required>
            </select>

            <label for="address_line1">Adress</label>
            <input type="text" name="address[line1]" id="address_line1" required value="<?php echo trim($address[0]) ?>" placeholder="Line 1" />
            <input type="text" name="address[line2]" id="address_line2" value="<?php echo isset($address[1])? trim($address[1]) : ''; ?>" placeholder="Line 2" />
            
            <label for="email">Email</label>
            <input type="email" name="email" id="email" required value="<?php echo $data['email'] ?>" placeholder="email@example.com" />

            <div id="error_messages"></div>
            
            <input type="hidden" name="action" id="action" value="Action\Update" />
            <input type="submit" value="Update">
            <a href="index.php">Cancel</a>

            <input type="hidden" name="id" value="<?php echo $data['id'] ?>" />
        </form>

        <script type="text/javascript">

        var canProceed = true;

        var validate = function(form) {
            // in case of non HTML5 browsers
            canProceed = true;
            $("#error_messages").html('');

            if (form.first_name.value == '') {
                showErrors('The First Name field can not be empty');
            }

            if (form.last_name.value == '') {
                showErrors('The Last Name field can not be empty');
            }

            if (form.country.value == '') {
                showErrors('The Country field can not be empty');
            }

            if (form.city.value == '') {
                showErrors('The City field can not be empty');
            }

            if (form.address_line1.value == '') {
                showErrors('The Address field can not be empty');
            }

            if (form.email.value == '') {
                showErrors('The Email field can not be empty');
            }

            pttr = /\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i;

            if (!pttr.test(form.email.value)) {
                showErrors('The Email field must be a valid email address');
            }

            return canProceed;
        }

        var showErrors = function (message) {
            $("#error_messages").html($("#error_messages").html() + '- ' + message + "<br />");
            canProceed = false;
        }

        var filterCity = function(value) {
            $.ajax({
                type: "GET",
                url: "/js/cities.json",
                cache: true,
                dataType: "json"
            }).done(function(json) {
                $("#city").html('');
                for (i in json[value]) {
                    if (i == "<?php echo $data['city'] ?>") {
                        selected = 'selected';
                    } else {
                        selected = '';
                    }

                    option = '<option value="' + i + '" ' + selected + '>' + json[value][i] + '</option>';
                    $("#city").append(option);
                }
            });
        }

        $(function () { 
            $.ajax({
                type: "GET",
                url: "/js/countries.json",
                cache: true,
                dataType: "json"
            }).done(function(json) {
                for (i in json) {
                    if (i == "<?php echo $data['country'] ?>") {
                        selected = 'selected';
                        filterCity(i)
                    } else {
                        selected = '';
                    }

                    option = '<option value="' + i + '" ' + selected + '>' + json[i] + '</option>';
                    console.debug(i, json[i]);
                    $("#country").append(option);
                }
            });
        });
        </script>
    </body>
</html>