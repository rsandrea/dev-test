<?php 
//ini_set('display_errors', 0);
function __autoload($class) {
    // convert namespace to full file path
    $class = str_replace('\\', '/', $class) . '.php';

    // print __DIR__ . '/../' . $class . "<br />";
    
    if (!in_array($class, get_declared_classes())) {
        require(realpath(__DIR__ . '/../' . $class));
    }
}

function fatalErrorHandler()
{
    # Getting last error
    $error = error_get_last();

    # Checking if last error is a fatal error 
    if(($error['type'] === E_ERROR) || ($error['type'] === E_USER_ERROR))
    {
         # Here we handle the error, displaying HTML, logging, ...
         echo 'You are trying to do something fishy... <b>Get back from whence you came!</b>';
    }
}
 
# Registering shutdown function
register_shutdown_function('fatalErrorHandler');

echo '<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>';